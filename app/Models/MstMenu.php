<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstMenu extends Model
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Append automatically
     *
     * @var array
     */
    protected $appends = [
        'label',
    ];

    /**
     * Accessors
     */
    public function getLabelAttribute()
    {
        return __("menu.{$this->name}");
    }

    /**
     * Relation
     */
    public function children()
    {
        return $this->hasMany('App\Models\MstMenu', 'parent_id');
    }

    /**
     * Scope
     */
    public function scopeByName($query, $name)
    {
        return $query->where('name', $name);
    }

    public function scopeFilterActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeParentOnly($query)
    {
        return $query->whereNull('parent_id');
    }
}
