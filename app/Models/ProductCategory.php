<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Scope
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeParent($query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Relation
     */
    public function children()
    {
        return $this->hasMany('App\Models\ProductCategory', 'parent_id');
    }
}
