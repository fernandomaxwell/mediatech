<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Scope
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }
}
