<?php

namespace App\Http\Controllers;

use App\Models\{
    Faq,
    ProductCategory,
    Setting
};
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(Request $request)
    {
        $product_categories = ProductCategory::select(['id', 'name', 'image'])
            ->active()
            ->parent()
            ->with(['children' => function ($query) {
                $query->select(['name', 'parent_id']);
            }])
            ->get();

        return view('home', compact(
            'product_categories'
        ));
    }

    public function career(Request $request)
    {
        return view('career');
    }

    public function faq(Request $request)
    {
        $vision = Setting::byKey('vision')->first();
        $mision = Setting::byKey('vision')->first();
        $faqs = Faq::active()->get();

        return view('faq', compact(
            'vision',
            'mision',
            'faqs'
        ));
    }
}
