<?php

namespace App\Http\Middleware;

use App\Models\MstMenu;
use Closure;

class GetMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $menus = MstMenu::select(['name', 'link', 'is_custom'])
            ->filterActive()
            ->parentOnly()
            ->with('children')
            ->get();

        $request->attributes->add(['menus' => $menus]);

        return $next($request);
    }
}
