<?php

use App\Models\Faq;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        Faq::truncate();

        // Data
        $data = [
            [
                'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing?',
                'answer' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
                'status' => 'active',
            ],
            [
                'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing?',
                'answer' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
                'status' => 'active',
            ],
            [
                'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing?',
                'answer' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
                'status' => 'active',
            ],
            [
                'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing?',
                'answer' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
                'status' => 'active',
            ],
            [
                'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing?',
                'answer' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
                'status' => 'active',
            ],
            [
                'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing?',
                'answer' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
                'status' => 'active',
            ],
        ];

        // Insert
        DB::beginTransaction();
        try {
            foreach ($data as $val) {
                Faq::create($val);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            echo $e->getMessage();
            die;
        }
    }
}
