<?php

use App\Models\ProductCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate
        ProductCategory::truncate();

        // Data
        $data = [
            [
                'name' => 'TRAVEL',
                'image' => 'img/product/travel.png',
                'sub_categories' => [
                    'Travel Adaptor',
                    'Crimping Tool',
                    'Measure',
                    'Power Bank',
                    'Bag',
                ],
            ],
            [
                'name' => 'COMPUTER & GADGET',
                'image' => 'img/product/gadget.png',
                'sub_categories' => [
                    'Laptop Desk',
                    'Keyboard & Mouse',
                    'Cable & Connector',
                    'Card Reader',
                    'Charger',
                    'Earphone',
                ],
            ],
            [
                'name' => 'GAMING',
                'image' => 'img/product/gaming.png',
                'sub_categories' => [
                    'Gaming Headset',
                    'Gaming Keyboard',
                    'Gaming Mouse',
                    'Gamepad',
                ],
            ],
            [
                'name' => 'CONSUMER ELECTRONIC',
                'image' => 'img/product/electronic.png',
                'sub_categories' => [
                    'Hydroponic Tools',
                    'Home Electronic',
                    'Inkjet Paper',
                ],
            ],
        ];

        // Insert
        DB::beginTransaction();
        try {
            foreach ($data as $category) {
                $product_category = ProductCategory::create(collect($category)->only(['name', 'image'])->toArray());
                foreach ($category['sub_categories'] as $sub_category) {
                    $product_category->children()->create(['name' => $sub_category]);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            echo $e->getMessage();
            die;
        }
    }
}
