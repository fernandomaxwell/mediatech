<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Data
        $data = [
            [
                'key' => 'asset_version',
                'value' => '1.0.11',
            ],
            [
                'key' => 'logo',
                'value' => 'img/logo.png',
            ],
            [
                'key' => 'vision',
                'value' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
            ],
            [
                'key' => 'mision',
                'value' => 'Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.',
            ],
        ];

        DB::beginTransaction();
        try {
            foreach ($data as $val) {
                $setting = Setting::byKey($val['key']);

                if ($setting->exists()) {
                    $setting->update(['value' => $val['value']]);
                } else {
                    Setting::create($val);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            echo $e->getMessage();
            die;
        }
    }
}
