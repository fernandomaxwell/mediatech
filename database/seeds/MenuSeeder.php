<?php

use App\Models\MstMenu;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Data
        $data = [
            [
                'name' => 'about_us',
                'link' => '#about-us',
                'parent_id' => null,
                'is_custom' => false,
                'status' => 'active',
            ],
            [
                'name' => 'our_product',
                'link' => '#our-product',
                'parent_id' => null,
                'is_custom' => false,
                'status' => 'active',
            ],
            [
                'name' => 'why_us',
                'link' => '#why-us',
                'parent_id' => null,
                'is_custom' => false,
                'status' => 'active',
            ],
            [
                'name' => 'testimonial',
                'link' => '#testimonial',
                'parent_id' => null,
                'is_custom' => false,
                'status' => 'active',
            ],
            [
                'name' => 'shop',
                'link' => '#',
                'parent_id' => null,
                'is_custom' => true,
                'status' => 'active',
            ],
        ];

        DB::beginTransaction();
        try {
            foreach ($data as $val) {
                $menu = MstMenu::byName($val['name']);

                if ($menu->exists()) {
                    $menu->update(collect($val)->except(['name'])->toArray());
                } else {
                    MstMenu::create($val);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            echo $e->getMessage();
            die;
        }
    }
}
