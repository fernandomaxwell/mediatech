<nav class="navbar navbar-expand navbar-dark">
    <ul class="navbar-nav ml-auto mr-lg-5">
        <li class="nav-item">
            <p class="nav-link active mx-lg-2">
                &copy; {{ date ('Y') }} {{ config('app.name') }}. Powered By DNA
            </p>
        </li>
        <li class="nav-item">
            <a class="nav-link active mx-lg-2" href="{{ url('/') }}#about-us">
                <span class='font-weight-bold'>About Us</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link active mx-lg-2" href="{{ url('career') }}">
                <span class='font-weight-bold'>Career</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link active mx-lg-2" href="{{ url('faq') }}">
                <span class='font-weight-bold'>FAQ</span>
            </a>
        </li>
    </ul>
</nav>