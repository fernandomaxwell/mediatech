<nav class="navbar navbar-expand-lg navbar-dark fixed-top @if(isset($optional)) {{ $optional }} @endif)">
    {{-- Hamburger Icon --}}
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class='container-fluid'>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            {{-- Logo --}}
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset(request()->get('logo')) }}" class='logo' alt="Logo">
            </a>

            {{-- Menu --}}
            <ul class='navbar-nav ml-auto mr-lg-4'>
                @if(request()->get('menus') !== null && !request()->get('menus')->isEmpty())
                    @foreach(request()->get('menus') as $menu)
                        <li class="nav-item my-auto mx-2">
                            @if($menu->is_custom)
                                <button type='button' class='btn btn-mediatech btn-mediatech-orange shadow'>
                                    <a class="nav-link active py-1 px-5" href="{{ $menu->link }}">
                                        <span class='font-weight-bold'>{{ $menu->label }}</span>
                                    </a>
                                </button>
                            @else
                                <a class="nav-link active" href="{{ url('/') . $menu->link }}">
                                    <span class='font-weight-bold'>{{ $menu->label }}</span>
                                </a>
                            @endif
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</nav>