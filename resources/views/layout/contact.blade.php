<div class='container-fluid contact-us-bottom my-5 my-lg-0'>
    <div class='container h-100'>
        <div class='row d-flex h-100'>
            <div class='col-lg-12 align-self-end'>
                <div class='row'>
                    <div class='col-lg-5'>
                        <h3 class='mb-4 text-white'>
                            <span class='font-weight-bold'>Contact</span> Us
                        </h3>

                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control form-control-lg border-0" placeholder="Name">
                            </div>
                            <div class="form-group my-4">
                                <input type="email" class="form-control form-control-lg border-0" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control border-0" rows="5"></textarea>
                            </div>
                            <button type='button' class='btn btn-mediatech-light-orange float-right px-5'>
                                <span class='font-weight-bold'>Send</span>
                            </button>
                        </form>
                    </div>

                    <div class='col-lg-2'></div>

                    <div class='col-lg-5'>
                        <div class='row d-flex h-100'>
                            <div class='col-lg-12 align-self-center'>
                                <img src="{{ asset('img/logo-white.png') }}">
                                <p class='text-white'>
                                    Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                </p>
                                <div style="height:120px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
