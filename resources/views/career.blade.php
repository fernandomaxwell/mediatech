@extends('layout.master')

@section('css')
    <style>
        .navbar-mediatech {
            background: transparent url("{{ asset('img/background/bg-navbar.png') }}") 0% 0% no-repeat;
        }

        .career-group-top {
            background: transparent url("{{ asset('img/background/bg-career-group-top.png') }}") 8% -250px no-repeat;
        }

        .career-group-top .banner {
            background: transparent url("{{ asset('img/background/bg-career-banner.png') }}") 0% 0% no-repeat;
        }

        .home-group-bottom {
            background: url("{{ asset('img/background/bg-home-group-bottom-1.png') }}"), url("{{ asset('img/background/bg-home-group-bottom-2.png') }}");
            background-position: center -550px, center center;
            background-repeat: no-repeat, no-repeat;
        }
    </style>
@endsection

@section('content')
    {{-- Navbar --}}
    @include('layout.navbar')

    {{-- Career Group Top --}}
    <div class='career-group-top'>
        {{-- Banner --}}
        <div class='container-fluid banner'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-12 text-white align-self-end'>
                        <div class='row'>
                            <div class='col-lg-6'>
                                <h2>
                                    Work with <span class='font-weight-bold'>Us</span>
                                </h2>
                                <p class='h4'>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing career@mediatech.com
                                </p>
                            </div>
                        </div>
                        <div class='row my-5'>
                            <div class='col-lg-12 mt-lg-5'>
                                <h3>
                                    <span class='font-weight-bold'>Job Available</span> Lorem ipsum dolor sit amet, consectetur
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Job --}}
        <div class='container-fluid job'>
            <div class='container'>
                <div class='row'>
                    <div class='col-lg-6 my-2'>
                        <div class="card card-mediatech border-0 shadow">
                            <div class="card-body mx-3">
                                <h5 class="text-mediatech-orange font-weight-bold my-3">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                </h5>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class='col-lg-6 my-2'>
                        <div class="card card-mediatech border-0 shadow">
                            <div class="card-body mx-3">
                                <h5 class="text-mediatech-orange font-weight-bold my-3">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                </h5>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class='col-lg-6 my-2'>
                        <div class="card card-mediatech border-0 shadow">
                            <div class="card-body mx-3">
                                <h5 class="text-mediatech-orange font-weight-bold my-3">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                </h5>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class='col-lg-6 my-2'>
                        <div class="card card-mediatech border-0 shadow">
                            <div class="card-body mx-3">
                                <h5 class="text-mediatech-orange font-weight-bold my-3">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                </h5>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>

                                <p class="card-text">
                                    <span class='h6 font-weight-bold'>Lmet, consectetur adipis:</span>
                                </p>

                                <ul class='px-4'>
                                    <li>Amet, consectetur adipis Lorem</li>
                                    <li>Lpsum dolor sit amet, consectetur adipiscing elit.</li>
                                    <li>Cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                                    <li>Adipiscing elit. cing elit. Amet, consectetur adipis</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Career Group Bottom --}}
    <div class='home-group-bottom'>
        {{-- How to Send CV --}}
        <div class='container-fluid'>
            <div class='container'>
                <div class='row my-5'>
                    <div class='col-lg-12'>
                        <div class='card card-mediatech border-0 shadow mt-0 mt-lg-5'>
                            <div class="card-body m-3">
                                <h3 class="text-mediatech-purple my-3">
                                    How to <span class='font-weight-bold'>Send CV</span>
                                </h3>

                                <ol class='px-3'>
                                    <li class='h5'>
                                        Lorem ipsu Lorem ipsum dolor sit amet, consectetur adipis Lorem ipsum dolor sit amet, m dolor sit amet, consectetur adipis Lorem ipsum dolor sit amet,
                                    </li>
                                    <li class='h5'>
                                        Lorem ipsum dolor sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                    </li>
                                    <li class='h5'>
                                        Lorem ipsum dolor sit amet, consectetur adipis Lorem ipsum dolor sit amet,
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Contact Us --}}
        @include('layout.contact')

        {{-- Footer --}}
        @include('layout.footer')
    </div>
@endsection

@section('javascript')
    <script>
        window.onscroll = function() {scrollFunction()}

        const scrollFunction = () => {
            if (document.body.scrollTop > 280 || document.documentElement.scrollTop > 280) {
                $('.fixed-top').addClass('navbar-mediatech py-lg-0')
            } else {
                $('.fixed-top').removeClass('navbar-mediatech py-lg-0')
            }
        }
    </script>
@endsection