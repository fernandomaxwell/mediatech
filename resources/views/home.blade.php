@extends('layout.master')

@section('css')
    <style>
        .fixed-top {
            background: transparent url("{{ asset('img/background/bg-navbar.png') }}") 0% 0% no-repeat;
        }

        .home-carousel {
            background: transparent url("{{ asset('img/background/bg-home-group-top-crop-1.png') }}") no-repeat;
        }

        .banner {
            background: url("{{ asset('img/background/bg-home-banner.png') }}") center no-repeat,
                url("{{ asset('img/background/bg-home-group-top-crop-2.png') }}") center no-repeat;
        }

        .about {
            background: url("{{ asset('img/background/bg-home-group-top-crop-3.png') }}") center 0% no-repeat,
                url("{{ asset('img/background/bg-home-group-top-2.png') }}") center -325px no-repeat,
                url("{{ asset('img/background/background_2_crop_1.png') }}") center 50px no-repeat;
        }

        .our-product {
            background: url("{{ asset('img/background/background_2_crop_2.png') }}") center 0% no-repeat;
        }

        .home-subgroup-top {
            background-image: url("{{ asset('img/background/background_3.png') }}");
            background-repeat: no-repeat;
            background-size: contain;
            background-position: 0% 61%;
        }

        .home-group-bottom {
            background: url("{{ asset('img/background/bg-home-group-bottom-1.png') }}") center -550px no-repeat,
                url("{{ asset('img/background/bg-home-group-bottom-2.png') }}") center center no-repeat;
        }

        @media (max-width: 576px) {
            .about {
                background: url("{{ asset('img/background/bg-home-group-top-crop-3.png') }}") center 0% no-repeat,
                    url("{{ asset('img/background/bg-home-group-top-2.png') }}") center -325px no-repeat,
                    url("{{ asset('img/background/background_2_crop_1.png') }}") center 65px no-repeat;
            }
        }
    </style>
@endsection

@section('content')
    {{-- Navbar --}}
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top py-lg-3">
        {{-- Hamburger Icon --}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class='container-fluid'>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                {{-- Logo --}}
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset(request()->get('logo')) }}" class='logo' alt="Logo">
                </a>

                {{-- Menu --}}
                <ul class='navbar-nav ml-auto mr-lg-4'>
                    @if(request()->get('menus') !== null && !request()->get('menus')->isEmpty())
                        @foreach(request()->get('menus') as $menu)
                            <li class="nav-item my-auto mx-2">
                                @if($menu->is_custom)
                                    <button type='button' class='btn btn-mediatech btn-mediatech-orange shadow'>
                                        <a class="nav-link active py-1 px-5" href="{{ $menu->link }}">
                                            <span class='font-weight-bold'>{{ $menu->label }}</span>
                                        </a>
                                    </button>
                                @else
                                    <a class="nav-link active" href="{{ $menu->link }}">
                                        <span class='font-weight-bold'>{{ $menu->label }}</span>
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    {{-- Home Group Top --}}
    <div class='home-group-top'>
        {{-- Carousel --}}
        <div class='container-fluid home-carousel'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-12 align-self-center align-self-lg-end'>
                        <div id="home-carousel" class="carousel slide text-center" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{ asset('img/carousel/bg-carousel-1.png') }}" class='w-100'>
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('img/carousel/bg-carousel-2.png') }}" class='w-100'>
                                </div>
                            </div>

                            <div class='mt-5'>
                                <ul class="carousel-indicators">
                                    <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#home-carousel" data-slide-to="1"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Banner --}}
        <div id='banner' class='container-fluid banner'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-8 text-white align-self-center'>
                        <h1 class='font-weight-bold'>
                            <span class='text-mediatech-orange'>Lifestyle</span> is
                            <div class='ml-lg-5'>your life</div>
                        </h1>

                        <h2 class='mt-4 mt-lg-5'>
                            Get what you need for
                            <div>
                                stylish items only at <span class='text-mediatech-pink font-weight-bold'>Mediatech</span>
                            </div>
                        </h2>

                        <a class="btn btn-mediatech btn-mediatech-orange my-4 my-lg-5 py-2 px-5" href="#">
                            <span class='font-weight-bold'>Shop Now</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        {{-- About --}}
        <div id='about-us' class='container-fluid about'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-12 my-5 py-5 align-self-center'>
                        <h3 class='text-center text-mediatech-orange'>
                            About <span class='font-weight-bold'>Us</span>
                        </h3>

                        <div class='row mt-3 mt-lg-5'>
                            <div class='col-lg-12'>
                                <div class='row mt-lg-5'>
                                    <div class='col-lg-6 order-lg-2'>
                                        <img src="{{ asset('img/icon/home-about-us.png') }}" class='w-100'>
                                    </div>

                                    <div class='col-lg-6 order-lg-1 mt-3 mt-lg-5'>
                                        <h3>
                                            <span class='text-mediatech-orange font-weight-bold'>Best, Sophisticated</span>
                                            <div>and trendy</div>
                                        </h3>

                                        <p class='h4 mt-4'>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquam purus a erat lobortis porta. Aliquam et luctus nisl. Maecenas et tellus velit.
                                        </p>

                                        <a class="btn btn-mediatech btn-mediatech-orange shadhow my-4 mb-lg-5 py-2 px-5" href="#">
                                            <span class='font-weight-bold'>Read More</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class='home-subgroup-top'>
            {{-- Our Product --}}
            <div class='container-fluid our-product'>
                <div class='container'>
                    <div class='row d-flex h-100'>
                        <div class='col-lg-12 my-5 py-5 text-center align-self-center'>
                            <div id='our-product' class='mt-5 py-5'>
                                <h3 class='text-mediatech-purple mt-4 pt-4'>
                                    Our <span class='font-weight-bold'>Product</span>
                                </h3>

                                <div class='row mt-3 mt-lg-5'>
                                    <div class='col-lg-8 offset-lg-2'>
                                        <p class='h4'>
                                            Lorem ipsum dolor sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                        </p>
                                    </div>
                                </div>

                                <div class='row mt-3 my-lg-5'>
                                    @foreach($product_categories as $product_category)
                                        <div class='col-6 col-lg-3 my-2 my-lg-0 category-product' data-img='{{ asset($product_category->image) }}'>
                                            {{-- Hover --}}
                                            <div class='row card-mediatech d-flex w-100 h-100' style="background: url('{{ asset($product_category->image) }}') center no-repeat">
                                                <div class='col-lg-12 align-self-center font-weight-bold text-white text-center hover-category-product'>
                                                    <div class='subcategory-product mt-3 d-none'>
                                                        @foreach($product_category->children as $sub_category)
                                                            <p class='my-0'>{{ $sub_category->name }}</p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Default --}}
                                            <div class='row w-100 default-category-product mt-3'>
                                                <div class='col-lg-12 font-weight-bold'>
                                                    {{ $product_category->name }}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Why Us --}}
            <div id='why-us' class='container-fluid why-us'>
                <div class='container h-100'>
                    <div class='row d-flex h-100'>
                        <div class='col-lg-12 my-lg-5 py-lg-5 align-self-center'>
                            <div class='row'>
                                <div class='col-lg-5'>
                                    <h3>
                                        Why <span class='font-weight-bold'>Us?</span>
                                    </h3>

                                    <p class='h4 mt-4'>
                                        Lorem ipsum dolor sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                    </p>
                                </div>
                            </div>

                            <div class='row mt-3 mt-lg-5 text-dark'>
                                <div class='col-lg-4 my-2 my-lg-0'>
                                    <div class="card card-mediatech border-0 shadow">
                                        <div class="card-body">
                                            <div class="outer-dot">
                                                <span class="dot mt-2"></span>
                                            </div>
                                            <h5 class="text-mediatech-purple font-weight-bold my-3">Card title</h5>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class='col-lg-4 my-2 my-lg-0'>
                                    <div class="card card-mediatech border-0 shadow">
                                        <div class="card-body">
                                            <div class="outer-dot">
                                                <span class="dot mt-2"></span>
                                            </div>
                                            <h5 class="text-mediatech-purple font-weight-bold my-3">Card title</h5>
                                            <p class="card-text mb-lg-5">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class='col-lg-4 my-2 my-lg-0'>
                                    <div class="card card-mediatech border-0 shadow">
                                        <div class="card-body">
                                            <div class="outer-dot">
                                                <span class="dot mt-2"></span>
                                            </div>
                                            <h5 class="text-mediatech-purple font-weight-bold my-3">Card title</h5>
                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Testimonial --}}
            <div id='testimonial' class='container-fluid testimonial my-5 my-lg-0'>
                <div class='container h-100'>
                    <div class='row d-flex h-100'>
                        <div class='col-lg-12 align-self-center'>
                            <div class="card card-mediatech border-0 shadow">
                                <div class="card-body text-center">
                                    <h3 class='text-mediatech-purple mt-4'>
                                        <span class='font-weight-bold'>Testimonial</span>
                                    </h3>

                                    <div class='row my-3 my-lg-4'>
                                        <div class='col-lg-8 offset-lg-2'>
                                            <p class='h4'>
                                                Lorem ipsum dolor sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                            </p>
                                        </div>
                                    </div>

                                    <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <div class='row'>
                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="carousel-item">
                                                <div class='row'>
                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="carousel-item">
                                                <div class='row'>
                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-lg-4'>
                                                        <div class='card border-0'>
                                                            <div class='card-body mx-3'>
                                                                <img src="{{ asset('img/user.png') }}" class='rounded-circle'>
                                                                <h5 class='font-weight-bold mt-5'>
                                                                    Lorem ipsum dolor
                                                                </h5>
                                                                <p>
                                                                    Lorem ipsum dolor Sit amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Testimonial carousel navigation --}}
                                            <a class="carousel-control-prev shadow" data-target="#testimonial-carousel" role="button" data-slide="prev">
                                                <img src="{{ asset('img/icon/back.png') }}">
                                            </a>
                                            <a class="carousel-control-next shadow" data-target="#testimonial-carousel" role="button" data-slide="next">
                                                <img src="{{ asset('img/icon/next.png') }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Home Group Bottom --}}
    <div class='home-group-bottom'>
        {{-- Go To Shop --}}
        <div class='container-fluid go-to-shop'>
            <div class='container h-100'>
                <div class='row'>
                    <div class='col-lg-5 text-center'>
                        <img src="{{ asset('img/icon/home-go-to-shop.png') }}">
                    </div>

                    <div class='col-lg-2'></div>

                    <div class='col-lg-4'>
                        <div class='row h-100'>
                            <div class='col-lg-12 align-self-center text-center text-lg-left'>
                                <h3 class='text-mediatech-orange font-weight-bold'>
                                    Come on..!!!
                                </h3>
                                <p class='h4 text-mediatech-orange font-weight-bold'>
                                    LET’S START TO SHOPPING
                                </p>
                                <a class="btn btn-mediatech btn-mediatech-orange shadhow my-4 mb-lg-5 py-2 px-5" href="#">
                                    <span class='font-weight-bold'>Go to Shop</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Contact Us --}}
        @include('layout.contact')

        {{-- Footer --}}
        <nav class="navbar navbar-expand navbar-dark">
            <ul class="navbar-nav ml-auto mr-lg-5">
                <li class="nav-item">
                    <p class="nav-link active mx-lg-2">
                        &copy; {{ date ('Y') }} {{ config('app.name') }}. Powered By DNA
                    </p>
                </li>
                <li class="nav-item">
                    <a class="nav-link active mx-lg-2" href="#about-us">
                        <span class='font-weight-bold'>About Us</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active mx-lg-2" href="{{ url('career') }}">
                        <span class='font-weight-bold'>Career</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active mx-lg-2" href="{{ url('faq') }}">
                        <span class='font-weight-bold'>FAQ</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
@endsection

@section('javascript')
    <script>
        window.onscroll = function() {scrollFunction()}
        window.onload = function() {scrollFunction()}

        const scrollFunction = () => {
            if (document.body.scrollTop > 280 || document.documentElement.scrollTop > 280) {
                $('.fixed-top').removeClass('py-lg-3').addClass('py-lg-0')
            } else {
                $('.fixed-top').removeClass('py-lg-0').addClass('py-lg-3')
            }
        }

        $('a[href*="#"]').on('click', function (e) {
            e.preventDefault()

            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top,
            }, 300, 'linear')
        })

        $('.category-product').mouseover(function () {
            let img = $(this).attr('data-img');
            $(this).find(".subcategory-product").removeClass('d-none')
            $(this).find('.card-mediatech').css('background', `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${img}) center no-repeat`)
        }).mouseout(function () {
            let img = $(this).attr('data-img');
            $(this).find(".subcategory-product").addClass('d-none')
            $(this).find('.card-mediatech').css('background', `url(${img}) center no-repeat`)
        })
    </script>
@endsection
