@extends('layout.master')

@section('css')
    <style>
        .fixed-top {
            background: transparent url("{{ asset('img/background/bg-navbar.png') }}") 0% 0% no-repeat;
        }

        .faq-group-top {
            background: url("{{ asset('img/background/bg-faq-group-top-1.png') }}?v={{ request()->get('asset_version') }}"),
                url("{{ asset('img/background/bg-faq-group-top-2.png') }}");
            background-position: -150px -320px, -200px -100px;
            background-repeat: no-repeat, no-repeat;
        }

        .faq-group-top .banner {
            background: url("{{ asset('img/logo-icon.png') }}") right 100px no-repeat;
            background-size: 49%;
        }

        .faq-group-top .vision-mision{
            background: transparent url("{{ asset('img/background/bg-faq.png') }}") center 420px no-repeat;
        }

        .faq {
            background: transparent url("{{ asset('img/background/bg-faq.png') }}") 0% -320px no-repeat;
        }

        .faq-group-bottom {
            background: url("{{ asset('img/background/bg-faq-group-bottom-1.png') }}"), url("{{ asset('img/background/bg-home-group-bottom-2.png') }}");
            background-position: center -550px, center center;
            background-repeat: no-repeat, no-repeat;
        }

        @media (max-width: 768px) {
            .faq-group-top .banner {
                background-position: center;
                background-size: contain;
            }
        }
    </style>
@endsection

@section('content')
    {{-- Navbar --}}
    @include('layout.navbar', ['optional' => 'py-lg-3'])

    {{-- FAQ Group Top --}}
    <div class='faq-group-top'>
        {{-- Banner --}}
        <div class='container-fluid banner'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-12 text-white align-self-center align-self-lg-end'>
                        <div class='row mt-lg-5 pt-lg-5'>
                            <div class='col-lg-6 text-center order-lg-3'>
                                <img src="{{ asset('img/icon/faq-laptop.png') }}">
                            </div>

                            <div class='col-lg-1 order-lg-2'></div>

                            <div class='col-lg-5 order-lg-1'>
                                <h3 class='mt-lg-5 pt-lg-5'>
                                    <span class='text-mediatech-orange font-weight-bold'>Best, Sophisticated</span>
                                    <div>and trendy</div>
                                </h3>

                                <p class='h4 mt-4 mt-lg-5'>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquam purus a erat lobortis porta. Aliquam et luctus nisl. Maecenas et tellus velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquam purus a erat lobortis porta.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Visi dan Misi --}}
        <div class='container-fluid vision-mision'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-6 align-self-top'>
                        <div class='card card-mediatech border-0 shadow mr-lg-4'>
                            <div class="card-body mx-3">
                                <h5 class="text-mediatech-purple font-weight-bold my-3">
                                    {{ strtoupper(__('wording.vision')) }}
                                </h5>
                                <p class="card-text mb-lg-5">
                                    {{ isset($vision) ? $vision->value : '' }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class='col-lg-6 align-self-top align-self-lg-center'>
                        <div class='card card-mediatech border-0 shadow ml-lg-4'>
                            <div class="card-body mx-3">
                                <h5 class="text-mediatech-purple font-weight-bold my-3">
                                    {{ strtoupper(__('wording.mision')) }}
                                </h5>
                                <p class="card-text mb-lg-5">
                                    {{ isset($mision) ? $mision->value : '' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- FAQ --}}
    <div class='container-fluid faq bg-white'>
        <div class='container h-100'>
            <div class='row d-flex h-100'>
                <div class='col-lg-12 my-5 mt-lg-0 text-white align-self-center'>
                    <div class='row'>
                        <div class='col-lg-12 text-center'>
                            <h3 class='font-weight-bold'>{{ ucwords(__('wording.faq')) }}</h3>
                        </div>
                    </div>

                    <div class='row my-5'>
                        @foreach($faqs as $faq)
                            <div class='col-lg-6 my-3'>
                                <div class='card-mediatech border border-white'>
                                    <div class='m-5'>
                                        <h5 class='font-weight-bold'>{{ $faq->question }}</h5>
                                        <p class='mt-4'>{{ $faq->answer }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class='container d-none d-lg-block' style='height: 40vh'></div>
    </div>

    {{-- FAQ Group Bottom --}}
    <div class='faq-group-bottom'>
        {{-- Go To Shop --}}
        <div class='container-fluid go-to-shop'>
            <div class='container h-100'>
                <div class='row h-100'>
                    <div class='col-lg-12'>
                        <div class='row'>
                            <div class='col-lg-5 text-center'>
                                <img src="{{ asset('img/icon/home-go-to-shop.png') }}">
                            </div>

                            <div class='col-lg-2'></div>

                            <div class='col-lg-4'>
                                <div class='row h-100'>
                                    <div class='col-lg-12 align-self-center text-center text-lg-left'>
                                        <h3 class='text-mediatech-orange font-weight-bold'>
                                            Come on..!!!
                                        </h3>
                                        <p class='h4 text-mediatech-orange font-weight-bold'>
                                            LET’S START TO SHOPPING
                                        </p>
                                        <a class="btn btn-mediatech btn-mediatech-orange shadhow my-4 mb-lg-5 py-2 px-5" href="#">
                                            <span class='font-weight-bold'>Go to Shop</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Contact Us --}}
        @include('layout.contact')

        {{-- Footer --}}
        @include('layout.footer')
    </div>
@endsection

@section('javascript')
    <script>
        window.onscroll = function() {scrollFunction()}
        window.onload = function() {scrollFunction()}

        const scrollFunction = () => {
            if (document.body.scrollTop > 280 || document.documentElement.scrollTop > 280) {
                $('.fixed-top').removeClass('py-lg-3').addClass('py-lg-0')
            } else {
                $('.fixed-top').removeClass('py-lg-0').addClass('py-lg-3')
            }
        }
    </script>
@endsection